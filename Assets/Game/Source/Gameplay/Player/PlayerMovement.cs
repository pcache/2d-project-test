﻿namespace Game.Gameplay.Player
{
    using Game.Services.Core;
    using Game.Services.Input;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerMovement : PlayerMovementBase
    {
        private PlayerInputBase playerInput;
        [SerializeField] private Rigidbody2D rb2d;

        private float mx = 0F, my = 0F;
        private Vector2 cVelocity;

        private void Awake()
        {
            playerInput = ServiceLocator.Instance.GetService<PlayerInputBase>();
        }

        // Update is called once per frame
        void Update()
        {
            var input = playerInput.GetInput();


            float dt = Time.deltaTime;

            float cSpeed = input.run == KeyState.pressed ? runSpeed : speed;

            var velocity = new Vector2(input.horizontal, input.vertical).normalized;
            velocity = velocity.normalized * cSpeed;
            velocity = Vector2.Lerp(cVelocity, velocity, dt * 25F);
            cVelocity = velocity;

            rb2d.velocity = velocity;

            Speed = velocity.magnitude;
        }

    }

}