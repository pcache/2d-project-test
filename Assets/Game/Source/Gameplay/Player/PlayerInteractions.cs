﻿namespace Game.Gameplay.Player
{
    using Game.Services.Core;
    using Game.Services.Input;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class PlayerInteractions : PlayerInteractionsBase
    {
        private PlayerInputBase playerInput;

        private void Awake()
        {
            playerInput = ServiceLocator.Instance.GetService<PlayerInputBase>();
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            var interactable = collision.GetComponent<IInteractable>();
            if (interactable != null && playerInput.GetInput().interact == KeyState.down)
            {
                interactable.OnInteraction();
            }
        }


    }

}