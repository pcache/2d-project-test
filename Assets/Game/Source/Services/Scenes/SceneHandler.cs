﻿namespace Game.Services.Scenes
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using SceneRef;
    using UnityEngine.SceneManagement;
    using Game.Events;
    using EventBus;
    using System;

    public class SceneHandler : SceneHandlerBase,
        IEventReceiver<OnSceneSwitchReady>,
        IEventReceiver<RequestSceneSwitch>
    {
        [SerializeField] private bool waitForSceneSwitchReady = true;

        private bool SceneSwitchReady;
        private SceneReference sceneToSwitchTo;
        private float maxSwitchWaitTime = 2F; // ensures we still switch if the event failed to raise

        private void Awake()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
            EventBus.Register(this);
        }

        private void OnDestroy()
        {
            EventBus.UnRegister(this);
        }

        public void OnEvent(OnSceneSwitchReady e) => SceneSwitchReady = true;
        public void OnEvent(RequestSceneSwitch e) => LoadScene(e.scene);
        private void OnSceneLoaded(Scene arg0, LoadSceneMode arg1) => EventBus<OnAfterSceneLoaded>.Raise();


        public override void LoadScene(SceneReference sceneRef)
        {
            sceneToSwitchTo = sceneRef;
            if (waitForSceneSwitchReady)
            {
                StopCoroutine(WaitForSceneSwitchReady());
                StartCoroutine(WaitForSceneSwitchReady());
            }

            EventBus<OnBeforeSceneLoaded>.Raise();

            if (!waitForSceneSwitchReady)
                CompleteSceneSwitch();
        }

        private void CompleteSceneSwitch()
        {
            SceneManager.LoadScene(sceneToSwitchTo.SceneIndex);
        }

        IEnumerator WaitForSceneSwitchReady()
        {
            float switchTime = Time.time + maxSwitchWaitTime;
            while (true)
            {
                if (SceneSwitchReady || Time.time > switchTime)
                {
                    SceneSwitchReady = false;
                    CompleteSceneSwitch();
                    yield break;
                }
                yield return null;
            }
        }

        
    }

}