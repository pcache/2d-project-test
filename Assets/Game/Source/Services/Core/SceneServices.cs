﻿namespace Game.Services.Core
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class SceneServices : MonoBehaviour
    {
        private void Reset()
        {
            gameObject.name = $"[{nameof(SceneServices)}]";
        }

        private void Awake()
        {
            var services = new List<IService>(GetComponentsInChildren<IService>());
            ServiceLocator.Instance.RegisterSceneServices(services, transform);
        }

        private void OnDestroy()
        {
            if(ServiceLocator.Instance != null)
                ServiceLocator.Instance.ClearSceneServices();
        }
    }

}