﻿namespace Game.Services.Core
{
    using Game.Utility;
    using Game.Utility.Singleton;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.SceneManagement;

    public interface IService
    {
    }


    public class ServiceLocator : MonoBehaviour, ISingletonInitializable
    {
        [SerializeField] private FallbackServices fallbackServices;
        public const string PERSISTENTS_PATH = "Game/Core/[Persistents]";

        private ServiceTypeMap fallbackServicesMap = new ServiceTypeMap();
        private ServiceTypeMap persistentServicesMap = new ServiceTypeMap();
        private ServiceTypeMap localSceneServicesMap = new ServiceTypeMap();

        private Transform persistentServicesRootTransform;
        private Transform sceneServicesRootTransform;

        private static bool applicationIsQuitting;

        // Manually implemented singleton for refined control
        private static ServiceLocator instance;
        public static ServiceLocator Instance
        {
            get
            {
                if (applicationIsQuitting)
                    return null;

                if (instance == null)
                {
                    instance = GameObject.FindObjectOfType<ServiceLocator>();
                    if (instance == null)
                    {
                        var go = GameObject.Instantiate(Resources.Load<GameObject>(PERSISTENTS_PATH));
                        instance = go.GetComponent<ServiceLocator>();
                    }
                }


                return instance;
            }
        }



        private class ServiceTypeMap
        {
            public Dictionary<Type, IService> serviceDictType = new Dictionary<Type, IService>();

            public T GetService<T>() where T : class, IService
            {
                if (this.serviceDictType.TryGetValue(typeof(T), out IService system))
                {
                    return system as T;
                }
                return null;
            }

            public void Add(IService service)
            {
                if (service == null)
                {
                    Debug.LogError($"{nameof(ServiceTypeMap)}: service should not be null");
                    return;
                }

                Type typekey = TypeUtility.FindFirstInterfaceOccurence<IService>(service.GetType());

                if (!this.serviceDictType.ContainsKey(typekey))
                {
                    this.serviceDictType.Add(typekey, service);
                }
                else
                {
                    //Debug.LogError($"{nameof(SystemsTypeMap)} : can't have 2 systems of the same base type");
                    return;
                }
            }



            public void Clear()
            {
                serviceDictType.Clear();
            }
        }

        void ISingletonInitializable.Initialize()
        {
            DontDestroyOnLoad(this);

            if (instance != null && (this != instance) && !applicationIsQuitting)
            {
                Destroy(this.gameObject);
            }

            EnsureSceneServicesObject();

            SceneManager.sceneLoaded -= OnSceneLoaded;
            SceneManager.sceneLoaded += OnSceneLoaded;

            Application.quitting += () => applicationIsQuitting = true;

            var persistentServices = GetComponentsInChildren<IService>();
            foreach (var item in persistentServices)
            {
                if (CheckMonoBehaviorServiceValidity(item))
                    persistentServicesMap.Add(item);
            }

            foreach (var item in fallbackServices.servicePrefabs)
            {
                fallbackServicesMap.Add(item.GetComponent<IService>());
            }

            persistentServicesRootTransform = this.transform;
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            EnsureSceneServicesObject();
        }

        private void EnsureSceneServicesObject()
        {
            var sceneServicesGo = GameObject.FindObjectOfType<SceneServices>();
            if (!sceneServicesGo)
            {
                var go = new GameObject($"[{nameof(SceneServices)}]");
                go.AddComponent<SceneServices>();
            }
        }

        private IService InitializeService(IService service)
        {
            var mb = service as MonoBehaviour;
            if (mb != null)
            {
                var go = GameObject.Instantiate(mb.gameObject);
                go.transform.SetParent(sceneServicesRootTransform);
                service = go.GetComponent<IService>();
                localSceneServicesMap.Add(service);
                return service;
            }

            return null;
        }

        private bool CheckMonoBehaviorServiceValidity(IService service)
        {
            MonoBehaviour monoBehaviour = service as MonoBehaviour;
            if (monoBehaviour)
            {
                return monoBehaviour.gameObject.activeSelf;
            }

            return true;
        }

        public T GetService<T>() where T : class, IService
        {
            var service = localSceneServicesMap.GetService<T>();
            if (service == null)
            {
                service = persistentServicesMap.GetService<T>();
                if (service == null)
                {
                    // If service was not found in active maps, we try to get it from the fallback list and instantiate it
                    service = fallbackServicesMap.GetService<T>();
                    if (service != null)
                    {
                        return InitializeService(service) as T;
                    }
                    else
                    {
                        Debug.LogError($"{nameof(ServiceLocator)} : service is not registered.");
                        return null;
                    }
                }
            }
            return service;
        }



        public void RegisterSceneServices(List<IService> services, Transform scenetransform)
        {
            sceneServicesRootTransform = scenetransform;
            foreach (var item in services)
            {
                if (CheckMonoBehaviorServiceValidity(item))
                    localSceneServicesMap.Add(item);
            }
        }

        public void ClearSceneServices()
        {
            localSceneServicesMap.Clear();
        }


    }

}