﻿namespace Game.Services.Input
{
    using Game.Services.Core;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;



    /// <summary>
    /// Provides input, and control over state (blocking, filtering)
    /// </summary>
    public class PlayerInputAlternative : PlayerInputBase
    {
        public bool PlayerInputActive { get; set; } = true;

        private long frame;
        private InputState lastInput;


        public override InputState GetInput()
        {
            if (!PlayerInputActive)
                return EmptyInputState;

            int currentFrame = Time.frameCount;

            if (currentFrame == frame)
                return lastInput;

            lastInput = new InputState()
            {
                vertical = Input.GetAxisRaw("Vertical"),
                horizontal = Input.GetAxisRaw("Horizontal"),
                lookX = Input.GetAxisRaw("Mouse X"),
                lookY = Input.GetAxisRaw("Mouse Y"),
                mousePosition = Input.mousePosition,
                attack = resolveKeyState(KeyCode.Mouse0),
                dash = resolveKeyState(KeyCode.Mouse1),
                run = resolveKeyState(KeyCode.LeftShift),
                weapon1 = resolveKeyState(KeyCode.Alpha1),
                weapon2 = resolveKeyState(KeyCode.Alpha2),
                mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition),
            };

            lastInput.hasMovementInput = lastInput.vertical != 0F || lastInput.horizontal != 0F;
            lastInput.hasLookInput = lastInput.lookX != 0F || lastInput.lookY != 0F;
            lastInput.movementVector = new Vector2(lastInput.horizontal, lastInput.vertical);
            lastInput.lookVector = new Vector2(lastInput.lookX, lastInput.lookY);

            frame = currentFrame;

            return lastInput;
        }

    }

}