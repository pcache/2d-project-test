﻿namespace Game.Services.GUI
{
    using DG.Tweening;
    using EventBus;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Game.Events;

    public class GUISceneSwitchFader : GUISceneSwitchFaderBase
        , IEventReceiver<OnBeforeSceneLoaded>
        , IEventReceiver<OnAfterSceneLoaded>
    {
        [SerializeField] protected float FadeTime = 0.5F;

        private void OnEnable()
        {
            EventBus.Register(this);   
        }

        private void OnDisable()
        {
            EventBus.UnRegister(this);
        }

        public override void Fade()
        {
            canvas.enabled = true;
            canvasGroup.alpha = 0;
            canvasGroup.blocksRaycasts = true;
            canvasGroup.DOFade(1F, FadeTime).OnComplete(() =>
            {
                EventBus<OnSceneSwitchReady>.Raise();
            });
        }

        public override void UnFade()
        {
            canvasGroup.alpha = 1F;
            canvasGroup.blocksRaycasts = false;
            canvasGroup.DOFade(0F, FadeTime).OnComplete(() =>
            {
                if(canvas)
                    canvas.enabled = false;
            });

        }

        public void OnEvent(OnBeforeSceneLoaded e)
        {
            Fade();
        }

        public void OnEvent(OnAfterSceneLoaded e)
        {
            UnFade();
        }
    }

}