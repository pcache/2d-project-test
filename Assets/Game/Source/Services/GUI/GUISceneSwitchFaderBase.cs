﻿namespace Game.Services.GUI
{
    using Game.Services.Core;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class GUISceneSwitchFaderBase : MonoBehaviour, IService
    {
        [SerializeField] protected CanvasGroup canvasGroup;
        [SerializeField] protected Canvas canvas;

        public abstract void Fade();
        public abstract void UnFade();

    }

}