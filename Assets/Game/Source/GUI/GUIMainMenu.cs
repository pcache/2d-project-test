﻿namespace Game.GUI
{
    using Game.Services.Core;
    using Game.Services.Scenes;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Game.Events;
    using EventBus;
    using Game.Data;

    public class GUIMainMenu : MonoBehaviour
    {

        private void Awake()
        {
        }

        public void LoadGameLevel()
        {
            EventBus<RequestSceneSwitch>.Raise(new RequestSceneSwitch()
            {
                scene = GameData.Instance.SceneData.SceneGame
            });
        }


    }

}