﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Game.Services.Core;

public static class MenuItems
{
    [MenuItem("Game/New Scene Services")]
    public static void CreateSceneServicesRoot()
    {
        var go = new GameObject();
        go.AddComponent<SceneServices>();
    }

    [MenuItem("Game/Select Persistents")]
    public static void SelectPersistents()
    {
        Selection.activeObject = Resources.Load<GameObject>(ServiceLocator.PERSISTENTS_PATH);
    }
}
