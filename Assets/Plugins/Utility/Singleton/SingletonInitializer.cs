﻿namespace Game.Utility.Singleton
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class SingletonInitializer : MonoBehaviour
    {
        private void Awake()
        {
            var singletons = GetComponents<ISingletonInitializable>();
            foreach (var item in singletons)
            {
                item.Initialize();
            }
        }
    }

}