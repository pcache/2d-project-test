﻿namespace Game.Utility
{


    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public static class ImmidiateSpriteDrawer
    {
        private static Material m_Material;

        public static void DrawSprite(Sprite sprite, Vector3 position)
        {
            if (m_Material == null)
                m_Material = new Material(Shader.Find("Sprites/Default"));

            if (sprite == null)
                return;

            var xyMin = Camera.current.WorldToScreenPoint(position + sprite.bounds.min);
            var xyMax = Camera.current.WorldToScreenPoint(position + sprite.bounds.max);

            var texW = sprite.texture.width;
            var texH = sprite.texture.height;
            var uvMin = new Vector2(sprite.rect.min.x / texW, sprite.rect.min.y / texH);
            var uvMax = new Vector2(sprite.rect.max.x / texW, sprite.rect.max.y / texH);


            GL.PushMatrix();

            GL.LoadPixelMatrix();

            m_Material.mainTexture = sprite.texture;
            m_Material.SetPass(0);

            GL.Begin(GL.QUADS);

            GL.MultiTexCoord(0, new Vector3(uvMin.x, uvMin.y, 0));
            GL.Vertex3(xyMin.x, xyMin.y, 0);

            GL.MultiTexCoord(0, new Vector3(uvMin.x, uvMax.y, 0));
            GL.Vertex3(xyMin.x, xyMax.y, 0);

            GL.MultiTexCoord(0, new Vector3(uvMax.x, uvMax.y, 0));
            GL.Vertex3(xyMax.x, xyMax.y, 0);

            GL.MultiTexCoord(0, new Vector3(uvMax.x, uvMin.y, 0));
            GL.Vertex3(xyMax.x, xyMin.y, 0);

            GL.End();

            GL.PopMatrix();
        }

    }

}