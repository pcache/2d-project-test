﻿public interface IReadOnlyArrayInternal<T>
{
    T[] GetArray();
    void SetArray(T[] array);
    void SetLength(int length);
}

public class ReadOnlyArray<T> : IReadOnlyArrayInternal<T>
{
    private T[] array;
    private int length = 0;
    public int Length => length;

    public T this[int i]
    {
        get { return array[i]; }
    }

    public ReadOnlyArray(int size)
    {
        array = new T[size];
    }

    T[] IReadOnlyArrayInternal<T>.GetArray()
    {
        return array;
    }

    void IReadOnlyArrayInternal<T>.SetArray(T[] array)
    {
        this.array = array;
    }

    void IReadOnlyArrayInternal<T>.SetLength(int length)
    {
        this.length = length;
    }
}
