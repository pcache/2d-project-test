﻿namespace Game.Utility.MonoFSM
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class MonoFSMStateBase : MonoBehaviour
    {
        public enum Status
        {
            active,
            inactive,
            transitioning
        }

        private Status _currentStatus;
        public Status CurrentStatus
        {
            get { return _currentStatus; }
            protected set { _currentStatus = value; }
        }

        public event Action OnStateEnter;
        public event Action OnStateExit;

        public void SetStateEnter(bool forced)
        {
            if (OnStateEnter != null)
                OnStateEnter();
            OnStateEnterHandler(forced);
        }

        public void SetStateExit(bool forced)
        {
            if (OnStateExit != null)
                OnStateExit();
            OnStateExitHandler(forced);
        }

        protected virtual void OnStateEnterHandler(bool forced) { }
        protected virtual void OnStateExitHandler(bool forced) { }

    }

}