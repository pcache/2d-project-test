﻿namespace Game.Utility.MonoFSM
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [System.Serializable]
    public class StateMapping
    {
        public string value;
        public MonoFSMStateBase state;
    }

    public class MonoFSM : MonoBehaviour, ISerializationCallbackReceiver
    {
        public event Action<string> OnStateChanged;

        public string CurrentStateKey { get; protected set; }
        public MonoFSMStateBase CurrentState { get; protected set; }

        [SerializeField]
        protected string InitialState;

        [SerializeField]
        protected List<StateMapping> stateMappings = new List<StateMapping>();

        protected Dictionary<string, MonoFSMStateBase> states = new Dictionary<string, MonoFSMStateBase>();

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            states.Clear();
            foreach (var s in stateMappings)
            {
                if (!states.ContainsKey(s.value))
                    states.Add(s.value, s.state);
            }
        }

        protected virtual void Awake()
        {
            ForceExitAllStates();
            RestoreLastState();
        }

        public void SetState(string state)
        {
            SetState(state, false);
        }

        public void SetState(string state, bool forced = false)
        {
            MonoFSMStateBase nextstate;
            if (states.TryGetValue(state, out nextstate))
            {
                if (CurrentState != null)
                {
                    CurrentState.SetStateExit(forced);
                }
                nextstate.SetStateEnter(forced);
                CurrentState = nextstate;
                CurrentStateKey = state;
                OnStateChanged?.Invoke(state);
            }
        }

        public void ForceExitAllStates()
        {
            foreach (var s in states)
            {
                s.Value.SetStateExit(true);
            }
        }

        public void RestoreLastState()
        {
            if (string.IsNullOrEmpty(CurrentStateKey))
            {
                CurrentStateKey = InitialState;
            }
            SetState(CurrentStateKey, true);
        }

        public void StoreStates(List<MonoFSMStateBase> list)
        {
            stateMappings.Clear();
            foreach (var item in list)
            {
                stateMappings.Add(new StateMapping()
                {
                    state = item,
                    value = CleanString(item.name)
                });
            }
        }

        private string CleanString(string str)
        {
            return str.Replace("[", "").Replace("]", "");
        }
    }

}