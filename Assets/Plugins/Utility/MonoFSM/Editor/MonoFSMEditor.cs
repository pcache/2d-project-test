﻿namespace Game.Utility.MonoFSM

{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(MonoFSM))]
    public class MonoFSMEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            MonoFSM monoFSM = target as MonoFSM;

            if (GUILayout.Button("Get Child States"))
            {
                List<MonoFSMStateBase> list = new List<MonoFSMStateBase>();
                foreach (Transform item in monoFSM.transform)
                {
                    var state = item.GetComponent<MonoFSMStateBase>();
                    if (state)
                        list.Add(state);
                }

                monoFSM.StoreStates(list);
            }
            base.OnInspectorGUI();
        }
    }

}