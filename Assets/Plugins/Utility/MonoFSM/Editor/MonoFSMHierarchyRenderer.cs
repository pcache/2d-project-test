﻿namespace Game.Utility.MonoFSM
{
    using System.Linq;
    using UnityEditor;
    using UnityEngine;

    [InitializeOnLoad]
    public class MonoFSMHierarchyRenderer
    {
        private static Vector2 offset = new Vector2(-25, 0);
        private static GUIStyle style;

        static MonoFSMHierarchyRenderer()
        {
            EditorApplication.hierarchyWindowItemOnGUI += HandleHierarchyWindowItemOnGUI;
        }

        private static void HandleHierarchyWindowItemOnGUI(int instanceID, Rect selectionRect)
        {
            var obj = EditorUtility.InstanceIDToObject(instanceID);
            if (!obj)
                return;

            var go = obj as GameObject;
            if (!go)
                return;

            var monostate = go.GetComponent<MonoFSMStateBase>();
            if (!monostate)
                return;

            if (style == null)
            {
                style = new GUIStyle(EditorStyles.label);
            }

            switch (monostate.CurrentStatus)
            {
                case MonoFSMStateBase.Status.active:
                {
                    style.normal.textColor = Color.green;
                }
                break;
                case MonoFSMStateBase.Status.inactive:
                {
                    style.normal.textColor = Color.red;
                }
                break;
                case MonoFSMStateBase.Status.transitioning:
                {
                    style.normal.textColor = Color.yellow;
                }
                break;
                default:
                    break;
            }


            //EditorGUI.DrawRect(selectionRect, new Color(.76f, .76f, .76f));
            Rect offsetRect = new Rect(selectionRect.position + offset, selectionRect.size);
            EditorGUI.LabelField(offsetRect, "█", style);

        }
    } 
}