﻿namespace Game.Utility
{

    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Reflection.Emit;
    using UnityEngine;

    public static class TypeUtility
    {
        private static Dictionary<Type, ObjectActivator> activators = new Dictionary<Type, ObjectActivator>();

        public static object CreateInstance(Type type)
        {
            if (!activators.TryGetValue(type, out ObjectActivator activator))
            {
                activator = CreateCtor(type);
                if(activator == null)
                {
                    Debug.LogError($"{nameof(TypeUtility)}, could not construct activator for type : {type.FullName}");
                    return null;
                }
                activators.Add(type, activator);
            }

            return activator();

        }

        private static ObjectActivator CreateCtor(Type type)
        {
            if (type == null)
            {
                throw new NullReferenceException("type");
            }
            ConstructorInfo emptyConstructor = type.GetConstructor(Type.EmptyTypes);
            if(emptyConstructor == null)
            {
                Debug.LogError($"{nameof(TypeUtility)}, no default constructor for type: {type.FullName}");
                return null;
            }

            var dynamicMethod = new DynamicMethod("CreateInstance", type, Type.EmptyTypes, true);
            ILGenerator ilGenerator = dynamicMethod.GetILGenerator();
            ilGenerator.Emit(OpCodes.Nop);
            ilGenerator.Emit(OpCodes.Newobj, emptyConstructor);
            ilGenerator.Emit(OpCodes.Ret);
            return (ObjectActivator)dynamicMethod.CreateDelegate(typeof(ObjectActivator));
        }

        private delegate object ObjectActivator();

        private static Type[] typeArgs1 = new Type[1];
        private static Type listType = typeof(List<>);

        public static Type MakeGenericListType(Type type)
        {
            typeArgs1[0] = type;
            return listType.MakeGenericType(typeArgs1);
        }

        public static Type MakeGenericType1(Type generic, Type arg)
        {
            typeArgs1[0] = arg;
            return generic.MakeGenericType(typeArgs1);
        }



        /// <summary>
        /// Alternative version of <see cref="Type.IsSubclassOf"/> that supports raw generic types (generic types without
        /// any type parameters).
        /// </summary>
        /// <param name="baseType">The base type class for which the check is made.</param>
        /// <param name="toCheck">To type to determine for whether it derives from <paramref name="baseType"/>.</param>
        public static bool IsSubclassOfRawGeneric(this Type toCheck, Type baseType)
        {
            while (toCheck != typeof(object))
            {
                Type cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
                if (baseType == cur)
                {
                    return true;
                }

                toCheck = toCheck.BaseType;
            }

            return false;
        }
        /// <summary>
        /// Returns the type on which the interface first occurs from bottom up
        /// </summary>
        /// <typeparam name="T">Interface type</typeparam>
        /// <param name="from"></param>
        /// <returns></returns>
        public static Type FindFirstInterfaceOccurence<T>(Type from) 
        {
            Type lastImplementing = null;

            void RecursiveSearch(Type t)
            {
                if (from.BaseType == typeof(object))
                    return;

                if (typeof(T).IsAssignableFrom(t))
                {
                    lastImplementing = t;
                    RecursiveSearch(t.BaseType);
                }
                else
                    return;
            }

            RecursiveSearch(from);

            return lastImplementing;
        }

    }

}