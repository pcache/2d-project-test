﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TextureUtilities  {

    public static void FillTexture(this Texture2D tex, Color color) {

        var pixels = tex.GetPixels();
        for (int i = 0; i < pixels.Length; i++) {
            pixels[i] = color;
        }

        tex.SetPixels(pixels);
        tex.Apply();

    }
	
}
