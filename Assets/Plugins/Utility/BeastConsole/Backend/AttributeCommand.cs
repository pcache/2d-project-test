﻿namespace BeastConsole.Backend.Internal
{
    using System;
    using System.Linq;
    using System.Reflection;
    using UnityEngine;

    internal class AttributeCommand : Command
    {

        internal MethodInfo m_methodInfo;
        internal ParameterInfo[] m_parameters;
        internal Type[] m_paramTypes;

        private object[] m_paramObj;
        private bool IsStatic;

        public AttributeCommand(string name, string description, ConsoleBackend backend) : base(name, description, backend)
        {
        }

        internal void Initialize(MethodInfo info)
        {
            this.IsStatic = info.IsStatic;
            this.m_methodInfo = info;
            this.m_parameters = info.GetParameters();
            this.m_paramTypes = this.m_parameters.Select(x => x.ParameterType).ToArray();
        }

        internal override void Execute(string line)
        {
            var split = line.TrimEnd().Split(' ');

            if (split.Length - 1 < this.m_parameters.Length)
            {
                BeastConsole.Console.WriteLine("Wrong parameters count");
                return;
            }

            if (this.m_paramObj == null)
                this.m_paramObj = new object[this.m_paramTypes.Length];

            for (int i = 0; i < this.m_paramTypes.Length; i++)
            {
                try
                {
                    this.m_paramObj[i] = Convert.ChangeType(split[i + 1], this.m_paramTypes[i]);
                }
                catch (Exception e)
                {
                    UnityEngine.Debug.LogError("param(" + i + ") -" + e.Message);
                }
                if (this.m_paramObj[i] == null)
                {
                    return;
                }
            }

            if (this.IsStatic)
            {
                this.m_methodInfo.Invoke(null, this.m_paramObj);
            }
            else
            {
                var gos = GameObject.FindObjectsOfType(this.m_methodInfo.DeclaringType);
                int count = gos.Length;
                for (int i = 0; i < count; i++)
                {
                    this.m_methodInfo.Invoke(gos[i], this.m_paramObj);
                }
            }

        }
    }
}